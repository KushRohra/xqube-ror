# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_04_01_113000) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "auth_tokens", force: :cascade do |t|
    t.integer "user_id"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payments", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "payment_date"
    t.float "payment_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "mode"
    t.integer "status"
    t.integer "created_by"
    t.integer "updated_by"
  end

  create_table "publishers", force: :cascade do |t|
    t.string "full_name"
    t.string "company_name"
    t.string "email_id"
    t.integer "property_monetize"
    t.text "property_name"
    t.integer "monthly_view"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "site_requests", force: :cascade do |t|
    t.integer "user_id"
    t.string "from_email"
    t.string "to_email"
    t.string "title"
    t.string "subject"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "created_by"
    t.integer "updated_by"
    t.index ["user_id"], name: "index_site_requests_on_user_id"
  end

  create_table "tags", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "name", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "encoded_id"
    t.boolean "is_active", default: true
    t.string "file_file_name"
    t.string "file_content_type"
    t.bigint "file_file_size"
    t.datetime "file_updated_at"
    t.integer "created_by"
    t.integer "updated_by"
  end

  create_table "tags_infos", force: :cascade do |t|
    t.integer "tag_id", null: false
    t.float "ad_request", null: false
    t.float "match_request", null: false
    t.float "clicks", null: false
    t.float "ctr", null: false
    t.float "ecpm", null: false
    t.float "partner_revenue", null: false
    t.date "frm_date", null: false
    t.date "to_date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "fill_rate"
    t.integer "created_by"
    t.integer "updated_by"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "encoded_id"
    t.string "username"
    t.string "first_name"
    t.string "last_name"
    t.boolean "is_active", default: true
    t.string "file_file_name"
    t.string "file_content_type"
    t.bigint "file_file_size"
    t.datetime "file_updated_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  add_foreign_key "payments", "users", column: "created_by"
  add_foreign_key "payments", "users", column: "updated_by"
  add_foreign_key "site_requests", "users", column: "created_by"
  add_foreign_key "site_requests", "users", column: "updated_by"
  add_foreign_key "tags", "users", column: "created_by"
  add_foreign_key "tags", "users", column: "updated_by"
  add_foreign_key "tags_infos", "users", column: "created_by"
  add_foreign_key "tags_infos", "users", column: "updated_by"
end
