class AddUserCreatedAndUpdatedToPayments < ActiveRecord::Migration[5.2]
  def change
    add_column :payments, :user_created_by, :integer
    add_column :payments, :user_updated_by, :integer
    add_foreign_key :payments, :users, column: :user_created_by
    add_foreign_key :payments, :users, column: :user_updated_by
  end
end
