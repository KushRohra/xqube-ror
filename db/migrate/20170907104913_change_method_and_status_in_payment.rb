class ChangeMethodAndStatusInPayment < ActiveRecord::Migration[5.2]

  def change
    change_column :payments, :payment_method, 'text USING CAST(payment_method AS text)'
    change_column :payments, :payment_status, 'text USING CAST(payment_status AS text)'
  end

end
