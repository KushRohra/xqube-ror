class ChangePaymentMethodInPayments < ActiveRecord::Migration[5.2]

  def change
    change_column :payments, :payment_method, 'integer USING CAST(payment_method AS integer)',:default => 1
  end

end
