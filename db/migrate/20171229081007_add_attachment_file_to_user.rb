class AddAttachmentFileToUser < ActiveRecord::Migration[5.2]
  def up
    add_attachment :users, :file
  end

  def down
    remove_attachment :users, :file
  end
end
