class AddUserCreatedAndUpdatedToTagsInfo < ActiveRecord::Migration[5.2]
  def change
    add_column :tags_infos, :user_created_by, :integer
    add_column :tags_infos, :user_updated_by, :integer
    add_foreign_key :tags_infos, :users, column: :user_created_by
    add_foreign_key :tags_infos, :users, column: :user_updated_by
  end
end
