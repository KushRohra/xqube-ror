class AddUserCreatedUpdatedBySitesRequest < ActiveRecord::Migration[5.2]
  def change
    add_column :site_requests, :user_created_by, :integer
    add_column :site_requests, :user_updated_by, :integer
    add_foreign_key :site_requests, :users, column: :user_created_by
    add_foreign_key :site_requests, :users, column: :user_updated_by
  end
end
