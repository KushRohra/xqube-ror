class CreateSiteRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :site_requests do |t|
      t.integer :user_id, index:true, foreign_key: true
      t.string :from_email
      t.string :to_email
      t.string :title
      t.string :subject

      t.timestamps null: false
    end
  end
end
