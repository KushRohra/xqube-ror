class AddFillRateToTagsinfo < ActiveRecord::Migration[5.2]
  def change
    add_column :tags_infos, :fill_rate, :integer
  end
end
