class ChangeUserCreatedAndUpdatedByColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :payments, :user_created_by, :created_by
    rename_column :payments, :user_updated_by, :updated_by
    rename_column :site_requests, :user_created_by, :created_by
    rename_column :site_requests, :user_updated_by, :updated_by
    rename_column :tags, :user_created_by, :created_by
    rename_column :tags, :user_updated_by, :updated_by
    rename_column :tags_infos, :user_created_by, :created_by
    rename_column :tags_infos, :user_updated_by, :updated_by
  end
end
