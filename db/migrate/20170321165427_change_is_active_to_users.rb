class ChangeIsActiveToUsers < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :is_active, :boolean, :default => false
  end
end
