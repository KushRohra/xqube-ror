class ChangePropertyMonetizeType < ActiveRecord::Migration[5.2]
  def change
    change_column :publishers, :property_monetize, 'integer USING CAST(property_monetize AS integer)'
  end
end
