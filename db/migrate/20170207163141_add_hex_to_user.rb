class AddHexToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :encoded_id, :string
    add_column :tags, :encoded_id, :string
  end
end
