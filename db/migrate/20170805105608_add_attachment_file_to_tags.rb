class AddAttachmentFileToTags < ActiveRecord::Migration[5.2]
  def self.up
    change_table :tags do |t|
      t.attachment :file
    end
  end

  def self.down
    remove_attachment :tags, :file
  end
end
