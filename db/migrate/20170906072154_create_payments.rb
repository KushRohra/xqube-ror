class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.integer :payment_id
      t.string :user_id
      t.datetime :payment_date
      t.float :payment_amount
      t.string :payment_method

      t.timestamps null: false
    end
  end
end
