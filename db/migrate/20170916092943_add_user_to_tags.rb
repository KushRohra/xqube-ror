class AddUserToTags < ActiveRecord::Migration[5.2]
  def change
    add_column :tags, :user_created_by, :integer
    add_foreign_key :tags, :users, column: :user_created_by
  end
end
