class RemovePaymentIdInPayments < ActiveRecord::Migration[5.2]
  def change
    remove_column :payments, :payment_id
  end
end
