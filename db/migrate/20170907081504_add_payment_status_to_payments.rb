class AddPaymentStatusToPayments < ActiveRecord::Migration[5.2]
  def change
    add_column :payments, :payment_status, :integer, :default => 1
  end
end
