class AddUserUpdatedToTags < ActiveRecord::Migration[5.2]
  def change
    add_column :tags, :user_updated_by, :integer
    add_foreign_key :tags, :users, column: :user_updated_by
  end
end
