class ChangeColumnDefaultInPayments < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:payments, :payment_method, nil)
    change_column_default(:payments, :payment_status, nil)
  end
end
