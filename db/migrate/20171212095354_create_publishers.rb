class CreatePublishers < ActiveRecord::Migration[5.2]
  def change
    create_table :publishers do |t|
      t.string :full_name
      t.string :company_name
      t.string :email_id
      t.boolean :property_monetize
      t.text :property_name
      t.boolean :monthly_view

      t.timestamps null: false
    end
  end
end
