class ChangeMonthlyViewsType < ActiveRecord::Migration[5.2]
  def change
    change_column :publishers, :monthly_view, 'integer USING CAST(monthly_view AS integer)'
  end
end
