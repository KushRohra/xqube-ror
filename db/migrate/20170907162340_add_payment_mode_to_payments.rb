class AddPaymentModeToPayments < ActiveRecord::Migration[5.2]
  def change
    add_column :payments, :mode, :integer
    add_column :payments, :status, :integer

    remove_column :payments ,:payment_method
    remove_column :payments ,:payment_status

  end
end
