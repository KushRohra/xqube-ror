# Adsolut Media ROR Production Project #

Integrations & Javascript Codes

1. Integrated ONE UI Theme.
2. Added Google Analytics, Hotjar Javascript code.


**Release V1.0**

1) Integrated Tags Summary & Tags_info details functionality.
2) Uploader functionality to upload stats daily.


**Release V2.0**

1) Added feature of super admin, admin, customer.
2) CRUD Functionality for User, Tags, Tags_infos.
3) Search on Tags page.
4) Integrated sendgrid emailer functionality for request sites via email.
5) Implemented static gravator feature for customer role.