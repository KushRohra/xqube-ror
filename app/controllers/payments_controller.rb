class PaymentsController < ApplicationController

  before_action :set_payment, only: [:show, :edit, :update, :destroy]

  # GET /payments
  # GET /payments.json
  def index

    @payments = {}

    if current_user.has_role? :admin

      add_breadcrumb 'Add Payments', :new_payment_path
      add_breadcrumb 'All Payments'

      @payments = Payment.includes(:user).search(params[:search]).page(params[:page]).per(30)

    else

      add_breadcrumb 'Summary', :root_path
      add_breadcrumb 'All Payments'

      @payments = Payment.joins(:user).where(:user_id => current_user.id).page(params[:page]).per(12)

      counts = Hash.new 0

      @payments.each do |payment|

        date = payment.payment_date.to_date

        amount = payment.payment_amount

        counts[payment.status] += 1

      end

      @count_status = counts

    end

  end

  # GET /payments/1
  # GET /payments/1.json
  def show
  end

  # GET /payments/new
  def new
    @payment = Payment.new
  end

  # GET /payments/1/edit
  def edit
    @payment = Payment.find(params[:id])
  end

  # POST /payments
  # POST /payments.json
  def create
    @payment = Payment.new(payment_params.merge(created_by: current_user.id))
    respond_to do |format|
      if @payment.save
        format.html { redirect_to payments_path, notice: 'Payment was successfully created.' }
        format.json { render :show, status: :created, location: @payment }
      else
        format.html { render :new }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    respond_to do |format|
      if @payment.update(payment_params.merge(updated_by: current_user.id))
        format.html { redirect_to payments_path, notice: 'Payment was successfully updated.' }
        format.json { render :show, status: :ok, location: @payment }
      else
        format.html { redirect_to edit_payment_path, alert: @payment.errors }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to payments_url, notice: 'Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_payment
    @payment = Payment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def payment_params
    params.require(:payment).permit(:payment_id, :user_id, :payment_date, :payment_amount, :mode, :status, :created_by, :updated_by)
  end

end
