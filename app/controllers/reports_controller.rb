class ReportsController < ApplicationController
  include ReportsHelper
  before_action :authenticate_user!
  respond_to :html, :js

  def index
    add_breadcrumb 'home', :root_path
    add_breadcrumb 'Websites', :tags_path
    add_breadcrumb 'Reports'

    @summary = {}
    @tags_infos = {}

    if params[:tag_id].present?

      @tag = Tag.find_by(id: params[:tag_id][:id])

      if @tag.present?
        @tags_infos = TagsInfo.includes(:tag).where(tag_id: params[:tag_id][:id]).where('DATE(frm_date) >= ? AND to_date <= ?', Date.strptime(params[:start_date], '%m/%d/%Y'), Date.strptime(params[:end_date], '%m/%d/%Y')).order(tag_id: :desc, frm_date: :asc).page(params[:page]).per(30)

        @summary = fetch_data(@tags_infos)
        flash.clear
      else
        flash[:error] = 'Invalid tag'
      end
    end

    respond_to do |format|
      format.html
      format.js
    end

  rescue => e
    flash[:error] = 'Something went wrong. Please drop us an email.'

    respond_to do |format|
      format.html
      format.js
    end
  end

  def download_csv
    tags_ids = params[:tag_ids].split(',')
    if tags_ids.present?
        @tags_infos = TagsInfo.includes(:tag).where(tag_id: tags_ids)
                          .where('DATE(frm_date) >= ? AND to_date <= ?',
                                 Date.strptime(params[:start_date], '%m/%d/%Y'),
                                 Date.strptime(params[:end_date], '%m/%d/%Y'))
                          .order(tag_id: :desc, frm_date: :asc)

        @summary = fetch_data(@tags_infos)

        send_data write_to_csv(@tags_infos, @summary), :type => "text/csv", :filename => "#{Date.today}_report.csv"
    end
  end

  protected

  def fetch_data(tags_infos)
    summary = {}
    total_ad_request = 0
    total_match_request = 0
    total_partner_revenue = 0
    avg_ecpm = []
    avg_fill_rate = []

    tags_infos.each do |x|
      total_ad_request = total_ad_request + x.ad_request.round(1)
      total_match_request = total_match_request + x.match_request.round(1)
      total_partner_revenue = total_partner_revenue + x.partner_revenue.round(1)
      avg_ecpm << x.ecpm
      avg_summary_ecpm = avg_ecpm.inject(0.0) { |sum, x| sum + x } / avg_ecpm.size
      avg_fill_rate << x.fill_rate
      avg_summary_fill_rate = avg_fill_rate.inject(0.0) { |sum, x| sum + x } / avg_fill_rate.size

      summary = { :total_ad_request => total_ad_request, :total_match_request => total_match_request,
                  :total_partner_revenue => total_partner_revenue, :total_ecpm => avg_summary_ecpm, :total_fill_rate => avg_summary_fill_rate }
    end
    summary
  end

end
