class PublishersController < ApplicationController
  before_action :authenticate_user!, :except => [:new,:create, :thank_you]

  def index
    @publishers = Publisher.all.page(params[:page]).per(20)
  end

  def new
  @publisher = Publisher.new
  end

  def create
    @publisher = Publisher.new(publisher_signup_params)
    respond_to do |format|
      if verify_recaptcha(model: @publisher) && @publisher.save
        format.html { redirect_to thank_you_publishers_path, notice: 'Thanks for Signing Up will reach out to you shortly !!' }
        format.json { render :show, status: :created, location: @publisher }
      else
        format.html { redirect_to new_publisher_path, alert: 'Unfortunately Due To Some Errors We Are Unable To Save Your Information. Please reach out to info@adsolut.in' }
        format.json { render json: @publisher.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  end

  def thank_you

  end

  private

  def publisher_signup_params
    params.require(:publisher).permit(:full_name, :company_name, :email_id, :property_monetize, :property_name, :monthly_view)
  end

end
