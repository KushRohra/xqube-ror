module Api
  class TagsController < ApplicationController
    before_action :authenticated_user!
    include ApplicationHelper

    include Swagger::Docs::ImpotentMethods
    respond_to :json
    swagger_controller :users, 'Tags'

    swagger_api :index do |api|
      summary "List user's tags"
      ApplicationController.add_auth_params(api)
      ApplicationController.add_response_params(api)
      response HTTP_STATUS_CODE_808, 'Invalid Password'
    end
    def index
      @data = Tag.where(user_id: @current_user.id)
      data = ActiveModel::ArraySerializer.new(@data, each_serializer: TagSerializer)
      send_response(HTTP_STATUS_CODE_200, true, I18n.t('index'), data)
    rescue => e
      send_response(HTTP_STATUS_CODE_500, false, e.message)
    end
  end
end