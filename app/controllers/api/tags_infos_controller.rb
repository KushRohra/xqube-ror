module Api
  class TagsInfosController < ApplicationController
    before_action :authenticated_user!

    def index
      @tag = Tag.find_by(:encoded_id => params[:tag_id])
      if @tag.present?
        start_date = params[:start_date].present? ? Date.strptime(params[:start_date], '%d/%m/%Y') : Date.today
        end_date = params[:end_date].present? ? Date.strptime(params[:end_date], '%d/%m/%Y') : Date.today
        @tags_info = TagsInfo.includes(:tag).where(tag_id: @tag.id)
                         .where('DATE(frm_date) >= ? AND to_date <= ?', start_date, end_date)
                         .order(tag_id: :desc, frm_date: :asc)
        data = ActiveModel::ArraySerializer.new(@tags_info, each_serializer: TagsInfoSerializer)
        send_response(HTTP_STATUS_CODE_200, true, I18n.t('index'), data)
      else
        send_response(HTTP_STATUS_CODE_404, false, I18n.t('tags_infos.error.not_found'))
      end
    rescue => e
      send_response(HTTP_STATUS_CODE_500, false, e.message)
    end
  end
end