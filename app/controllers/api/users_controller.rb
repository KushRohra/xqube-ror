# frozen_string_literal: true

module Api
  class UsersController < ApplicationController
    include Swagger::Docs::ImpotentMethods
    before_action :authenticated_user!, only: [:sign_out]
    respond_to :json
    swagger_controller :users, 'Users'

    swagger_api :sign_in do |api|
      summary 'User Sign-in'
      param :form, 'user[username]', :string, :required, 'Username'
      param :form, 'user[password]', :string, :required, 'User Password'
      ApplicationController.add_response_params(api)
      response HTTP_STATUS_CODE_808, 'Invalid Password'
    end
    def sign_in
      required_params_present = check_required_params([:user])
      if required_params_present
        response = User.authenticate_user(params[:user])
        send_response(response[:status], response[:success], response[:message], response[:data])
      else
        send_response(HTTP_STATUS_CODE_801, false, I18n.t('params_missing.error'))
      end
    rescue => e
      send_response(HTTP_STATUS_CODE_500, false, e.message)
    end

    swagger_api :sign_out do |api|
      summary 'User Sign out'
      ApplicationController.add_auth_params(api)
      ApplicationController.add_response_params(api)
    end
    def sign_out
      token = @current_user.auth_tokens.find_by(token: request.headers['HTTP_AUTH_TOKEN'])
      if token.destroy
        send_response(HTTP_STATUS_CODE_200, true, I18n.t('users.success.user_sign_out'))
      end
    rescue => e
      send_response(HTTP_STATUS_CODE_500, false, e.message)
    end

    private

    def user_params
      params.require(:user).permit([:email, :password, :first_name, :last_name, :phone, :image, :status, :organization_id, :gender])
    end
  end
end
