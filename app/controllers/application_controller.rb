class ApplicationController < ActionController::Base
  include Pundit
  # protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  # Common Error Codes
  HTTP_STATUS_CODE_200 = 200 # success response
  HTTP_STATUS_CODE_202 = 202 # success response for quotes
  HTTP_STATUS_CODE_401 = 401 # Authentication failed
  HTTP_STATUS_CODE_403 = 403 # Unauthorized access
  HTTP_STATUS_CODE_404 = 404 # Record not found
  HTTP_STATUS_CODE_500 = 500 # Exception found error
  HTTP_STATUS_CODE_801 = 801 # Invalid params
  HTTP_STATUS_CODE_850 = 850 # Can not delete as already used
  HTTP_STATUS_CODE_950 = 950 # Auth Token invalid
  HTTP_STATUS_CODE_808 = 808 # Invalid password

  def after_sign_in_path_for(resource)
    if current_user.is_active
      if current_user.has_role? :admin
        tags_path
      else
        pages_path
      end
    else
      sign_out
      flash[:error] = 'User is deactivated.'
      user_session_path
    end
  end

  def check_required_params(required_params_list)
    missing_params_list = []
    required_params_list.each do |field|
      missing_params_list << field unless params[field].present?
    end
    missing_params_list.empty?
  end

  def self.add_response_params(api)
    api.response :ok, 'Success'
    api.response :not_found
    api.response 500, 'Internal server error'
    api.response 801, 'Parameters missing'
  end

  def self.add_auth_params(api)
    api.param :header, 'AUTH_TOKEN', :string, :required, 'Access token for authentication'
  end

  def send_response(status, success, message = '', data = [])
    data = [] unless data.present?
    result = {
        status: status,
        success: success,
        message: (message.present?) ? message.gsub('Validation failed: ', '') : '',
        data: data,
    }
    render json: result
  end

  def authenticated_user!
    if request.headers['HTTP_AUTH_TOKEN'].present?
      token = AuthToken.find_by(token: request.headers['HTTP_AUTH_TOKEN'])
      if token.present?
        @current_user = token.user
      else
        send_response(HTTP_STATUS_CODE_950, false, I18n.t('users.error.token_invalid'))
      end
    else
      send_response(HTTP_STATUS_CODE_801, false, I18n.t('auth_token_header_missing'))
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :first_name, :last_name, :email, :password])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :username, :email, :password, :password_confirmation, :current_password])

  end
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
end