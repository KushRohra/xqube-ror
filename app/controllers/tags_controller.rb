class TagsController < ApplicationController
  before_action :authenticate_user!
  include ApplicationHelper

  def index
    if current_user.has_role? :admin
      @tags = Tag.joins(:user).search(params[:search]).order('is_active DESC').page(params[:page]).per(10)
      add_breadcrumb 'Add Website', new_tag_path
    else
      @tags = Tag.joins(:user).search(params[:search]).where(:user_id => current_user.id).order(name: :asc).page(params[:page]).per(10)
      add_breadcrumb 'home', :root_path
      add_breadcrumb 'Websites', :tags_path
    end
  end

  def show
    if current_user.has_role? :customer
      tags = Tag.joins(:user).find_by(:encoded_id => params[:encoded_id])

      @tags_breadcrumb = tags

      add_breadcrumb 'home', :root_path
      add_breadcrumb 'Websites', :tags_path
      add_breadcrumb "#{@tags_breadcrumb.name}", :tag_path

      # Add Rows and Values
      data_table = GoogleVisualr::DataTable.new

      data_table.new_column('date', 'Date')
      data_table.new_column('number', 'Impressions')
      data_table.new_column('number', 'Earnings')

      @tags_infos = TagsInfo.where(:tag_id => tags.id).includes(:tag => :tags_infos).order(frm_date: :desc).page(params[:page]).per(10)

      @tags_infos.each do |tag_info|
        data_table.add_rows([[tag_info.frm_date, tag_info.ad_request, tag_info.partner_revenue]])
      end

      opts = {:width => 750, :height => 354, animation: {startup: true, duration: 1000, easing: 'out'}, :title => 'Total Ad Revenue', :curveType => 'function', hAxis: {title: 'Date', titleTextStyle: {color: '#000000'}}, series: {0 => {targetAxisIndex: 0}, 1 => {targetAxisIndex: 1}}, vAxes: {0 => {title: 'Impressions'}, 1 => {title: 'dollars', format: '$#,###'}}}

      @chart = GoogleVisualr::Interactive::LineChart.new(data_table, opts)
    else
      @tag = Tag.find_by(:encoded_id => params[:encoded_id])
      @tags_infos = TagsInfo.where(:tag_id => @tag.id).order(frm_date: :desc).page(params[:page]).per(10)
    end


  end

  def export

    tags = Tag.joins(:user).find_by(:encoded_id => params[:encoded_id])

    @tags_infos = TagsInfo.where(:tag_id => tags.id).includes(:tag => :tags_infos).order(frm_date: :desc)
    respond_to do |format|
      format.html
      format.csv { send_data @tags_infos.to_csv, filename: "Report-#{tags.name}-#{Date.today}.csv" }
    end

  end

  def new
    @tag = Tag.new
  end

  def create
    begin
      Tag.create!(tag_params.merge(created_by: current_user.id))
      flash[:notice] = "Tag successfully created."
      redirect_to tags_path
    rescue => err # rescues all exceptions
      logger.error(err.to_s)
      flash[:error] = err.to_s
      redirect_to new_tag_path
    end
  end

  def edit
    @tag = Tag.find_by(:encoded_id => params[:encoded_id])
  end

  def update
    @tag = Tag.find_by(:encoded_id => params[:encoded_id])
    if update_tag
      flash[:notice] = "Tag successfully updated."
      redirect_to tags_path
    else
      flash[:error] = @tag.errors
      redirect_to edit_tag_path
    end
  end

  def destroy
    @tag = Tag.find_by(:encoded_id => params[:encoded_id])
    @tag.destroy
    flash[:success] = "Record deleted succesfully"
    redirect_to tags_path
  end

  private

  def update_tag
    @tag = Tag.find(params[:encoded_id])

    param = params[:tag][:is_active]

    if param.present?
      if param == '1'
        value = true
      else
        value = false
      end
      form_params = {:is_active => value}

    else
      form_params = tag_params
    end


    return @tag.update!(form_params.merge(updated_by: current_user.id))
  end

  def tag_params
    params.require(:tag).permit(:name, :user_id, :is_active, :file, :created_by, :updated_by)
  end

end