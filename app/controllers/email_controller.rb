class EmailController < ApplicationController
  include ApplicationHelper

  def index
    add_breadcrumb 'home', :root_path
    add_breadcrumb 'Websites', :tags_path
    add_breadcrumb 'Request Tags'
  end

  def sending
    begin
      from = current_user.email
      to = 'info@adsolut.in'
      subject = params[:subject]
      body = params[:body]

      response = ApplicationMailer.email(from, to, subject, body)
      flash[:message] = 'Your email was sent successfully.'
      flash[:class] = 'alert-success'
    rescue Exception => e
      flash[:message] = e.message
      flash[:class] = 'alert-error'
    end

    redirect_to root_url
  end
end
