class SitesController < ApplicationController
  before_action :authenticate_user!
  autocomplete :tag, :name, :full => true
  respond_to :html, :js

  def index
    add_breadcrumb 'home', :root_path
    add_breadcrumb 'Websites', :tags_path
    add_breadcrumb 'Summary'

    @summary = {}

    @tags_infos ={}

    data_rows = []

    if params[:sites_name].blank? && params[:date_range].blank?
      tags = Tag.where(:is_active => true)
      tag_info = TagsInfo.where(:tag_id => tags.ids).where(:frm_date => 1.week.ago..Date.today).order(frm_date: :desc).group_by(&:frm_date)
    else
      if params[:tag_id].blank?
        tags = Tag.where(:is_active => true)
      else
        tags = Tag.where(id: params[:tag_id])
      end
      tag_info = TagsInfo.where(:tag_id => tags.ids, :frm_date => from_date).order(frm_date: :desc).group_by(&:frm_date)
    end

    tag_info.each do |key, data|
      match_request = data.collect { |x| x.match_request }.inject(0.0) { |sum, x| sum + x }
      partner_revenue = data.collect { |x| x.partner_revenue }.inject(0.0) { |sum, x| sum + x }
      ad_request = data.collect { |x| x.ad_request }.inject(0.0) { |sum, x| sum + x }
      data_rows << [key, match_request, partner_revenue, ad_request]
      @tags_infos = {:match_request => match_request, :partner_revenue => partner_revenue}
    end

    @chart = line_chart(data_rows)

    flash.clear

    respond_to do |format|
      format.html
      format.js
    end
  rescue => e
    flash[:error] = 'Something went wrong. Please drop us an email.'
    respond_to do |format|
      format.html
      format.js
    end

  end

  private

  def from_date
    case params[:date_range]
      when '7'
        1.week.ago..Date.today
      when '15'
        2.week.ago..Date.today
      else
        1.month.ago..Date.today
    end
  end

  def line_chart(data_rows)

    # Add Rows and Values
    data_table = GoogleVisualr::DataTable.new

    data_table.new_column('date', 'Date')
    data_table.new_column('number', 'Impressions')
    data_table.new_column('number', 'Earnings')
    data_table.new_column('number', 'Ad Request')


    data_table.add_rows(data_rows)

    opts = {:width => 900, :height => 500, animation: {startup: true, duration: 1000, easing: 'out'}, :title => 'Total Ad Revenue', :curveType => 'function', hAxis: {title: 'Date', titleTextStyle: {color: '#000000'}}, series: {0 => {targetAxisIndex: 0}, 1 => {targetAxisIndex: 1}}, vAxes: {0 => {title: 'Impressions'}, 1 => {title: 'dollars', format: '$#,###'}}}

    @line_chart = GoogleVisualr::Interactive::LineChart.new(data_table, opts)

    return @line_chart

  end

end
