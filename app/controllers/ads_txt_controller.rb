require "open-uri"
require "openssl"
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
require "net/http"
require "uri"

class AdsTxtController < ApplicationController
  before_action :authenticate_user!

  def download

    add_breadcrumb 'home', :root_path
    add_breadcrumb 'Websites', :tags_path
    add_breadcrumb 'ads.txt'

    begin

      @contents = ''

      s3_url = ''

      user = User.where(:id => current_user.id).where(:is_active => true)

      user.each do |user_data|
        if user_data.file.present?
          s3_url = user_data.file.url
        else
          s3_url = ''
        end
      end

      if !s3_url.empty?
        uri = URI.parse(s3_url.to_s)
        @contents = uri.read
      else
        @contents= ''
      end

    rescue OpenURI::HTTPError
      retry if (s3_url.any?)
      nil
    end
  end
end
