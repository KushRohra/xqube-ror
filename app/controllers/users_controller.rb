class UsersController < ApplicationController

  def show
    @user = User.find_by(:encoded_id => params[:encoded_id])
  end

  def index
    @users = User.search(params[:search]).order(created_at: :desc).page(params[:page]).per(10)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    begin
      if @user.save!
        @user.add_role params[:user][:role]
        flash[:notice] = "User successfully created."
        redirect_to users_path
      else
        flash[:error] = @user.errors
        redirect_to new_user_path
      end
    rescue => err # rescues all exceptions
      logger.error(err.to_s)
      flash[:error] = err.to_s
      redirect_to new_user_path
    end
  end

  def edit
    @user = User.find(params[:encoded_id])
  end

  def update
    @user = User.find(params[:encoded_id])
    if update_user
      flash[:notice] = "User successfully updated."
      redirect_to users_path
    else
      flash[:error] = @user.errors
      redirect_to edit_user_path
    end
  end

  def destroy
    User.find(params[:encoded_id]).destroy
    flash[:success] = "User successfully deleted."
    redirect_to users_path
  end

  private

  def user_params
    params.require(:user).permit(:username, :first_name, :last_name, :email, :password, :password_confirmation, :current_password, :file)
  end

  def update_user
    @user = User.find(params[:encoded_id])
    param = params[:user][:is_active]

    if param.present?
      if param == '1'
        value = true
      else
        value = false
      end
      form_params = {:is_active => value}
    else
      form_params = user_params
    end
    return @user.update!(form_params)
  end

end
