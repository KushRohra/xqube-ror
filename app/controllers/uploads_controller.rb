class UploadsController < ApplicationController
  before_action :authenticate_user!

  def new
  end

  def import
    begin
      spreadsheet = open_spreadsheet(params[:file])
      #excel_file = Roo::Spreadsheet.open(params[:file].path, extension: :xlsx)
      sheet_1 = spreadsheet.sheet('Sheet1')

      sheet_1.each_with_index(tag_name: 'tag_name', ad_request: 'ad_request', match_request: 'matched_request', clicks: 'clicks', ctr: 'ctr', ecpm: 'ecpm', fill_rate: 'fill_rate', partner_revenue: 'partner_revenue', frm_date: 'from_date', to_date: 'to_date') do |hash, index|

        unless params[:file].nil?
          unless index.zero?
            tag = Tag.find_or_create_by(name: hash[:tag_name], user_id: params[:user_id])
            tag.tags_infos.find_or_create_by(ad_request: hash[:ad_request],
                                             match_request: hash[:match_request],
                                             clicks: hash[:clicks].to_i,
                                             ctr: hash[:ctr].to_i,
                                             ecpm: hash[:ecpm],
                                             fill_rate: hash[:fill_rate],
                                             partner_revenue: hash[:partner_revenue].round(2),
                                             frm_date: hash[:frm_date],
                                             to_date: hash[:to_date])
          end
        end
      end
    rescue => err # rescues all exceptions
      logger.error(err.to_s)
      flash[:error] = err.to_s
      redirect_to new_upload
    end

    redirect_to root_url
  end


  def open_spreadsheet(file)
    case File.extname(file.original_filename)
      when '.csv' then
        Roo::CSV.new(file.path)
      when '.xls' then
        Roo::Excel.new(file.path)
      when '.xlsx' then
        Roo::Excelx.new(file.path)
      else
        raise 'Unknow file type: #{file.original_filename} '
    end
  end
end
