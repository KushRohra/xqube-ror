class PagesController < ApplicationController
  before_action :authenticate_user!
  include ApplicationHelper

  add_breadcrumb 'home', :root_path
  add_breadcrumb 'Websites', :tags_path
  add_breadcrumb 'All Websites'

  def index

    @tag_infos = {}

    @summary = {}

    @summary_tag_infos_week = {}

    total_ad_request = 0
    total_match_request = 0
    total_partner_revenue = 0
    total_ad_request_week = 0
    total_match_request_week = 0
    total_partner_revenue_week = 0
    avg_ecpm = []
    avg_fill_rate = []
    avg_fill_rate_week = []
    pie_chart_data_rows = []

    tags = Tag.where(:user_id => current_user.id).where(:is_active => true).includes(:user => :tags)

    tags_infos = TagsInfo.where(:tag_id => tags.ids).includes(:tag => :tags_infos).group_by(&:tag)

    tags_infos.each do |tag, tagsinfo|

      tag_name = tag.name
      tag_encoded_id = tag.encoded_id
      ad_request_sum = tagsinfo.collect { |x| x.ad_request }.inject(0.0) { |sum, x| sum + x }
      match_request_sum = tagsinfo.collect { |x| x.match_request }.inject(0.0) { |sum, x| sum + x }
      partner_revenue_sum = tagsinfo.collect { |x| x.partner_revenue }.inject(0.0) { |sum, x| sum + x }
      ecpm = tagsinfo.collect { |x| x.ecpm }
      ecpm_average = ecpm.inject(0.0) { |sum, x| sum + x } / ecpm.size
      fill_rate = tagsinfo.collect { |x| x.fill_rate.to_i }
      fill_rate_average = fill_rate.inject(0) { |sum, x| sum + x } / fill_rate.size
      frm_dates = tagsinfo.collect { |x| x.frm_date }
      to_dates = tagsinfo.collect { |x| x.to_date }
      range = "#{frm_dates.min} - #{to_dates.max}"

      @tag_infos[tag.id] = {:range => range, :name => tag.name, :ad_request => ad_request_sum, :match_request => match_request_sum, :partner_revenue => partner_revenue_sum, :ecpm => ecpm_average, :fill_rate => fill_rate_average, :tag_encoded_id => tag_encoded_id}

      pie_chart_data_rows << [tag_name,partner_revenue_sum]

      total_ad_request = total_ad_request + ad_request_sum
      total_match_request = total_match_request + match_request_sum
      total_partner_revenue = total_partner_revenue + partner_revenue_sum
      avg_ecpm << ecpm_average
      avg_summary_ecpm = avg_ecpm.inject(0.0) { |sum, x| sum + x } / avg_ecpm.size
      avg_fill_rate << fill_rate_average
      avg_summary_fill_rate = avg_fill_rate.inject(0.0) { |sum, x| sum + x } / avg_fill_rate.size

      @summary = {:total_ad_request => total_ad_request, :total_match_request => total_match_request, :total_partner_revenue => total_partner_revenue, :total_ecpm => avg_summary_ecpm, :total_fill_rate => avg_summary_fill_rate}
    end

    tags_infos_chart = TagsInfo.where(:tag_id => tags.ids, :frm_date => 1.week.ago..Date.today).order(frm_date: :desc).group_by(&:frm_date)

    data_rows = []

    tags_infos_chart.each do |from_date, tag_info_chart|

      ad_request = tag_info_chart.collect { |x| x.ad_request }.inject(0.0) { |sum, x| sum + x }

      partner_revenue = tag_info_chart.collect { |x| x.partner_revenue }.inject(0.0) { |sum, x| sum + x }

      match_request = tag_info_chart.collect { |x| x.match_request }.inject(0.0) { |sum, x| sum + x }

      fill_rate = tag_info_chart.collect { |x| x.fill_rate.to_i }

      fill_rate_average = fill_rate.inject(0) { |sum, x| sum + x } / fill_rate.size

      total_ad_request_week = total_ad_request_week + ad_request

      total_match_request_week = total_match_request_week + match_request

      total_partner_revenue_week = total_partner_revenue_week + partner_revenue

      avg_fill_rate_week << fill_rate_average

      avg_summary_fill_rate_week = avg_fill_rate_week.inject(0.0) { |sum, x| sum + x } / avg_fill_rate_week.size

      data_rows << [from_date, ad_request, partner_revenue]

      @summary_tag_infos_week = {:total_ad_request_week => total_ad_request_week, :total_match_request_week => total_match_request_week, :total_partner_revenue_week => total_partner_revenue_week, :avg_summary_fill_rate_week => avg_summary_fill_rate_week}
    end

    @chart = line_chart(data_rows)

    @revenue_chart = pie_chart(pie_chart_data_rows)

  end

  def line_chart(data_rows)

    # Add Rows and Values
    data_table = GoogleVisualr::DataTable.new

    data_table.new_column('date', 'Date')
    data_table.new_column('number', 'Impressions')
    data_table.new_column('number', 'Earnings')

    data_table.add_rows(data_rows)

    opts = {:height => 500, animation: {startup: true, duration: 1000, easing: 'out'}, :title => 'Total Ad Revenue', :curveType => 'function', hAxis: {title: 'Date', titleTextStyle: {color: '#000000'}}, series: {0 => {targetAxisIndex: 0}, 1 => {targetAxisIndex: 1}}, vAxes: {0 => {title: 'Impressions'}, 1 => {title: 'dollars', format: '$#,###'}}}

    @line_chart = GoogleVisualr::Interactive::LineChart.new(data_table, opts)

    return @line_chart

  end

  def pie_chart(pie_chart_data_rows)

    data_table = GoogleVisualr::DataTable.new

    data_table.new_column('string', 'Sites')
    data_table.new_column('number', 'Earnings')
    data_table.add_rows(pie_chart_data_rows)

    opts   = { :height => 500, :is3D => true }

    @pie_chart = GoogleVisualr::Interactive::PieChart.new(data_table, opts)

    return @pie_chart

  end


  def search_index
    @tags_infos = TagsInfo.joins(:tag).all.search(params[:search]).search_by_start_date(params[:start_date]).search_by_end_date(params[:end_date]).page(params[:page]).per(10)
  end

end


