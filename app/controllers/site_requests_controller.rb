class SiteRequestsController < ApplicationController
  before_action :authenticate_user!

  def index

  end

  def new
    add_breadcrumb 'home', :root_path
    add_breadcrumb 'Websites', :tags_path
    add_breadcrumb 'Request Tags'
    @site_request = SiteRequest.new
  end

  def create

    @site_request = SiteRequest.new(site_request_params.merge(created_by: current_user.id))

    @site_request.user_id = current_user.id

    @site_request.from_email = current_user.email

    @site_request.to_email = 'info@adsolut.in'

    @site_request.title = 'Please approve sites for' +' '+ @site_request.from_email

    if @site_request.save!
      ApplicationMailer.email(@site_request).deliver_now
      flash[:notice] = 'Your email was sent successfully.'
      redirect_to new_site_request_path
    else
      flash[:error] = @site_request.errors
      redirect_to new_site_request_path
    end
  end

  def show

  end

  private

  def site_request_params
    params.require(:site_request).permit(:user_id, :from_email, :to_email, :title, :subject, :created_by, :updated_by)
  end

end
