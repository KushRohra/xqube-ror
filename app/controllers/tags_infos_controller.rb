class TagsInfosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_tags_info, :only => [:edit, :update, :destroy]

  def new
    @tag = Tag.find_by(:encoded_id => params[:tag_id])
    @tags_info = @tag.tags_infos.new
  end


  def create
    @tags_info = TagsInfo.new(tags_info_params.merge(created_by: current_user.id))

    @tag = @tags_info.tag

    if @tags_info.save!
      flash[:notice] = "Information successfully created."
      redirect_to tag_path(encoded_id: @tag.encoded_id)
    else
      flash[:error] = @tags_info.errors
      redirect_to new_tags_info_path(encoded_id: @tag.encoded_id)
    end
  end


  def edit
    @tag = @tags_info.tag
  end

  def update
    if @tags_info.update(tags_info_params.merge(updated_by: current_user.id))
      flash[:notice] = "Information successfully updated."
      redirect_to tag_path(encoded_id: @tags_info.tag.encoded_id)
    else
      flash[:error] = @user.errors
      redirect_to new_tags_info_path(encoded_id: @tags_info.tag.encoded_id)
    end
  end

  def destroy
    @tags_info = TagsInfo.find(params[:id])
    @tags_info.destroy
    flash[:success] = "Record deleted succesfully"
    redirect_to tag_path(encoded_id: @tags_info.tag.encoded_id)
  end

  private

  def tags_info_params
    params.require(:tags_info).permit(:tag_id, :ad_request, :match_request, :clicks, :ctr, :ecpm, :partner_revenue, :frm_date, :to_date, :fill_rate, :created_by, :updated_by)
  end

  def set_tags_info
    @tags_info = TagsInfo.find(params[:id])
  end
end