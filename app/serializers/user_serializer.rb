# frozen_string_literal: true

class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :first_name, :last_name, :username, :auth_token

  def auth_token
    tokens = object.auth_tokens.last
    tokens.token if tokens.present?
  end
end
