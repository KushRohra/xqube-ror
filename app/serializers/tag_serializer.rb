# frozen_string_literal: true

class TagSerializer < ActiveModel::Serializer
  attributes :encoded_id, :name, :is_active, :file_url

  def file_url
    object.file.url
  end
end
