# frozen_string_literal: true

class TagsInfoSerializer < ActiveModel::Serializer
  attributes :frm_date, :to_date, :ad_request, :match_request, :ecpm, :fill_rate, :partner_revenue
end