class Payment < ActiveRecord::Base
  belongs_to :user

  enum mode: ['PayPal', 'eCheck', 'Wire Transfer']

  enum status: ['Paid', 'Pending']


  def self.search(search)
    if search
      where('email LIKE ?', "%#{search.downcase}%")
    else
      all
    end
  end

end
