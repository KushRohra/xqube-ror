class AuthToken < ActiveRecord::Base
  belongs_to :user

  def self.create_auth_token(user)
    payload = { timestamp: Time.now, username: user.username }
    secret_key = Rails.application.secrets.secret_key_base
    loop do
      @token = JWT.encode(payload, secret_key, 'HS256')
      break unless user.auth_tokens.exists?(token: @token)
    end
    user.auth_tokens.create!(token: @token)
    { status: 200, success: true, message: I18n.t('users.success.user_sign_in') }
  rescue => e
    { status: 500, success: false, message: e.message }
  end
end