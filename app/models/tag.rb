class Tag < ActiveRecord::Base
  belongs_to :user
  has_many :tags_infos, dependent: :destroy

  before_validation :generate_hex, :on => :create
  validates :user_id, presence: true
  validates :name, presence: true, length: {minimum: 3, maximum: 50}

  validates :file,
            attachment_content_type: {content_type: %w(text/plain text/html application/pdf application/xls application/xlsx application/doc application/docx application/vnd.ms-excel application/vnd.openxmlformats-officedocument.spreadsheetml.sheet application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document), message: ' Only PDF, EXCEL, WORD or TEXT files are allowed.'
            },
            attachment_size: {less_than: 5.megabytes, :message => ' File must be less than 5 megabytes in size '}

  has_attached_file :file

  def generate_hex
    loop do
      self.encoded_id = SecureRandom.hex
      break unless self.class.exists?(encoded_id: encoded_id)
    end
  end


  def self.to_csv
    CSV.generate do |csv|
      csv << column_names
      all.each do |result|
        csv << result.attributes.values_at(*column_names)
      end
    end
  end
  
 
  def self.search(search)
    if search.present?
      where(' LOWER(name) LIKE ? ', "%#{search.downcase}%")
    else
      all
    end
  end
end