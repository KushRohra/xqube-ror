class Publisher < ActiveRecord::Base
  paginates_per 25
  enum property_monetize: ['Rich Media Banner', 'In-App', 'In-Line Video / Outstream Video', 'Playstream In-Banner Video','Live Stream Ad Insertion']
  enum monthly_view: ['0..100,000', '100,000..1 Million', '1 Million +']

=begin
  VALID_EMAIL_REGEX= /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
=end

  validates_format_of :email_id, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create

  validates :email_id, presence: true, length: {maximum: 105}, uniqueness: {case_sensitive: false}

  validates :property_name, presence: true, length: {maximum: 250}

  before_save :downcase_fields

  private
  def downcase_fields
    self.email_id.downcase!
    self.property_name.downcase!
  end

end
