class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :tags

  has_many :auth_tokens

  validates :username, presence: true, uniqueness: {case_sensitive: false}, length: {minimum: 3, maximum: 25}

  VALID_EMAIL_REGEX= /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :email, presence: true, length: {maximum: 105}, uniqueness: {case_sensitive: false}, format: {with: VALID_EMAIL_REGEX}

  before_validation :generate_hex, :on => :create

  before_save :downcase_fields

  attr_accessor :role

  validates :file,
            attachment_content_type: {content_type: %w(text/plain text/html), message: 'Only TEXT files are allowed.'
            },
            attachment_size: {less_than: 5.megabytes, :message => ' File must be less than 5 megabytes in size '}

  has_attached_file :file

  def generate_hex
    loop do
      self.encoded_id = SecureRandom.hex
      break unless self.class.exists?(encoded_id: encoded_id)
    end
  end

  def full_name
    "#{self.first_name.titleize} #{self.last_name.titleize}"
  end

  def self.search(search)
    if search
      where('email LIKE ?', "%#{search.downcase}%")
    else
      all
    end
  end

  def self.authenticate_user(params)
    user = User.find_by(username: params[:username])
    if user.present?
      if user.valid_password?(params[:password])
        auth_response = AuthToken.create_auth_token(user)
        data = UserSerializer.new(user)
        { status: auth_response[:status], success: auth_response[:success], message: auth_response[:message], data: data }
      else
        { status: 808, success: false, message: I18n.t('users.error.invalid_password') }
      end
    else
      { status: 404, success: false, message: I18n.t('users.error.user_not_found') }
    end
  end

  private
  def downcase_fields
    self.email.downcase!
    self.username.downcase!
  end

end
