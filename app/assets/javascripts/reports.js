$(document).ready(function () {
    $(function () {
        App.initHelpers(['datepicker']);
    });

    $('#date_range').on('change', function() {
        var date_range = $(this).val();


        var start_date = Date.new;
        var end_date = Date.new;

        switch(date_range) {
            case 'custom':
                var yesterday = new Date();
                yesterday.setDate(yesterday.getDate() - 1);
                start_date = yesterday;
                end_date = yesterday;
                break;
            case 'today':
                start_date = new Date();
                end_date = new Date();
                break;
            case 'yesterday':
                var yesterday = new Date();
                yesterday.setDate(yesterday.getDate() - 1);
                start_date = yesterday;
                end_date = yesterday;
                break;
            case 'this_week':
                var dateNow = new Date();
                var firstDayOfTheWeek = (dateNow.getDate() - dateNow.getDay());
                start_date = new Date(dateNow.getFullYear(), dateNow.getMonth(), firstDayOfTheWeek);
                end_date = dateNow;
                break;
            case 'past_7_days':
                var oneWeekAgo = new Date();
                var epoch_date = oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);
                start_date = epoch_to_date(epoch_date);
                end_date = new Date();
                break;
            case 'this_month':
                var dateNow = new Date();
                var firstDayThisMonth = new Date(dateNow.getFullYear(), dateNow.getMonth(), 1);
                start_date = firstDayThisMonth;
                end_date = new Date();
                break;
            case 'past_30_days':
                var pastMonth = new Date();
                var epoch_date = pastMonth.setDate(pastMonth.getDate() - 30);
                start_date = epoch_to_date(epoch_date);
                end_date = new Date();
                break;
            case 'past_month':
                var dateNow = new Date();
                var firstDayLastMonth = new Date(dateNow.getFullYear(), dateNow.getMonth() -1 , 1);
                start_date = firstDayLastMonth;
                var lastDayLastMonth = new Date(dateNow.getFullYear(), dateNow.getMonth(), 0);
                console.log('prod check');
                end_date = lastDayLastMonth
                break;
            case 'past_90_days':
                var pastMonth = new Date();
                var epoch_date = pastMonth.setDate(pastMonth.getDate() - 90);
                start_date = epoch_to_date(epoch_date);
                end_date = new Date();
                break;
            case 'this_year':
                var dateNow = new Date();
                var firstDayThisYear = new Date(dateNow.getFullYear(), 0, 1);
                start_date = firstDayThisYear;
                end_date = new Date();
                break;
            case 'past_365_days':
                var past365days = new Date();
                var epoch_date = past365days.setDate(past365days.getDate() - 365);
                start_date = epoch_to_date(epoch_date);
                end_date = new Date();
                break;
            default:
                console.log('Invalid date');
        }

        $("#report_start_date").datepicker('setDate', start_date);

        $("#report_end_date").datepicker('setDate', end_date);

        // TODO Following setting date is not working
        $('#report_start_date').attr('value', start_date.toLocaleDateString());

        $("#report_end_date").attr('value', end_date.toLocaleDateString());

        validate_for_download_csv();

    });

    $('#tag_id_id').on('change', function() {
        validate_for_download_csv();
    })
});

function epoch_to_date(timetoken) {
    var utcSeconds = Math.round(timetoken / 1000);
    var date = new Date(0); // The 0 there is the key, which sets the date to the epoch
    date.setUTCSeconds(utcSeconds);
    return date
}

function validate_for_download_csv(){
    var tag = $('#tag_id_id :selected').val();
    var start_date = $('#report_start_date').val();
    var end_date = $('#report_end_date').val();

    if (tag == null || start_date == null || end_date == null){
        $('.download_csv').attr('disabled');
    } else {
        var tag_ids = [];
        $.each($("#tag_id_id option:selected"), function(){
            tag_ids.push($(this).val());
        });

        $('#tag_ids').val(tag_ids.toString());
        $('#start_date').val(start_date);
        $('#end_date').val(end_date);
        $('.download_csv').removeAttr('disabled');
    }
}