/*
 *  Document   : base_forms_wizard.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Form Wizard Page
 */

var BaseFormWizard = function () {
    // Init simple wizard, for more examples you can check out http://vadimg.com/twitter-bootstrap-wizard-example/
    var initWizardSimple = function () {
        jQuery('.js-wizard-simple').bootstrapWizard({
            'tabClass': '',
            'firstSelector': '.wizard-first',
            'previousSelector': '.wizard-prev',
            'nextSelector': '.wizard-next',
            'lastSelector': '.wizard-last',
            'onTabShow': function ($tab, $navigation, $index) {
                var $total = $navigation.find('li').length;
                var $current = $index + 1;
                var $percent = ($current / $total) * 100;

                // Get vital wizard elements
                var $wizard = $navigation.parents('.block');
                var $progress = $wizard.find('.wizard-progress > .progress-bar');
                var $btnPrev = $wizard.find('.wizard-prev');
                var $btnNext = $wizard.find('.wizard-next');
                var $btnFinish = $wizard.find('.wizard-finish');

                // Update progress bar if there is one
                if ($progress) {
                    $progress.css({width: $percent + '%'});
                }
                console.log($current >= $total);
                // If it's the last tab then hide the last button and show the finish instead
                if ($current >= $total) {
                    console.log('last');
                    $btnNext.hide();
                    $btnFinish.show();
                } else {
                    $btnNext.show();
                    $btnFinish.hide();
                }
            }
        });
    };

    // Init wizards with validation, for more examples you can check out http://vadimg.com/twitter-bootstrap-wizard-example/
    var initWizardValidation = function () {
        // Get forms
        var $form1 = jQuery('.js-form1');
        var $form2 = jQuery('.js-form2');

        // Prevent forms from submitting on enter key press
        $form1.add($form2).on('keyup keypress', function (e) {
            var code = e.keyCode || e.which;

            if (code === 13) {
                e.preventDefault();
                return false;
            }
        });

        // Init form validation on classic wizard form
        var $validator1 = $form1.validate({
            errorClass: 'help-block animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function (error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function (e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            success: function (e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            rules: {
                'validation-classic-firstname': {
                    required: true,
                    minlength: 2
                },
                'validation-classic-lastname': {
                    required: true,
                    minlength: 2
                },
                'validation-classic-email': {
                    required: true,
                    email: true
                },
                'validation-classic-details': {
                    required: true,
                    minlength: 5
                },
                'validation-classic-city': {
                    required: true
                },
                'validation-classic-skills': {
                    required: true
                },
                'validation-classic-terms': {
                    required: true
                },
                'inventory[po_number]': {
                    required: true,
                    maxlength: 25
                },
                'validate_sku': {
                    required: true
                },
                'inventory[quantity]': {
                    required: true,
                    number: true
                },
                'inventory[unit]': {
                    required: true
                },
                'order[quantity]': {
                    required: true,
                    number: true
                },
                'order[unit]': {
                    required: true
                },
                'order[store_keeper_id]': {
                    required: true
                }
            },
            messages: {
                'validation-classic-firstname': {
                    required: 'Please enter a firstname',
                    minlength: 'Your firtname must consist of at least 2 characters'
                },
                'validation-classic-lastname': {
                    required: 'Please enter a lastname',
                    minlength: 'Your lastname must consist of at least 2 characters'
                },
                'validation-classic-email': 'Please enter a valid email address',
                'validation-classic-details': 'Let us know a few thing about yourself',
                'validation-classic-skills': 'Please select a skill!',
                'validation-classic-terms': 'You must agree to the service terms!',
                'inventory[po_number]': {
                    required: 'You must enter PO number.',
                    maxlength: 'Should be less than 25 characters'
                },
                'validate_sku': {
                    required: 'Please select SKU'
                },
                'inventory[quantity]': {
                    required: 'Please enter quantity',
                    number: 'Please enter number only'
                },
                'inventory[unit]': {
                    required: 'Please select unit'
                },
                'order[quantity]': {
                    required: 'Please enter quantity',
                    number: 'Please enter number only'
                },
                'order[unit]': {
                    required: 'Please select unit'
                },
                'order[store_keeper_id]': {
                    required: 'Please select storekeeper'
                }
            }
        });

        // Init form validation on the other wizard form
        //var $validator2 = $form2.validate({
        //    errorClass: 'help-block text-right animated fadeInDown',
        //    errorElement: 'div',
        //    errorPlacement: function (error, e) {
        //        jQuery(e).parents('.form-group > div').append(error);
        //    },
        //    highlight: function (e) {
        //        jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
        //        jQuery(e).closest('.help-block').remove();
        //    },
        //    success: function (e) {
        //        jQuery(e).closest('.form-group').removeClass('has-error');
        //        jQuery(e).closest('.help-block').remove();
        //    },
        //    rules: {
        //        'validation-firstname': {
        //            required: true,
        //            minlength: 2
        //        },
        //        'validation-lastname': {
        //            required: true,
        //            minlength: 2
        //        },
        //        'validation-email': {
        //            required: true,
        //            email: true
        //        },
        //        'validation-details': {
        //            required: true,
        //            minlength: 5
        //        },
        //        'validation-city': {
        //            required: true
        //        },
        //        'validation-skills': {
        //            required: true
        //        },
        //        'validation-terms': {
        //            required: true
        //        }
        //    },
        //    messages: {
        //        'validation-firstname': {
        //            required: 'Please enter a firstname',
        //            minlength: 'Your firtname must consist of at least 2 characters'
        //        },
        //        'validation-lastname': {
        //            required: 'Please enter a lastname',
        //            minlength: 'Your lastname must consist of at least 2 characters'
        //        },
        //        'validation-email': 'Please enter a valid email address',
        //        'validation-details': 'Let us know a few thing about yourself',
        //        'validation-skills': 'Please select a skill!',
        //        'validation-terms': 'You must agree to the service terms!'
        //    }
        //});

        // Init classic wizard with validation
        jQuery('.js-wizard-classic-validation').bootstrapWizard({
            'tabClass': '',
            'previousSelector': '.wizard-prev',
            'nextSelector': '.wizard-next',
            'onTabShow': function ($tab, $nav, $index) {
                var $total = $nav.find('li').length;
                var $current = $index + 1;

                // Get vital wizard elements
                var $wizard = $nav.parents('.block');
                var $btnNext = $wizard.find('.wizard-next');
                var $btnFinish = $wizard.find('.wizard-finish');

                //If it's the last tab then hide the last button and show the finish instead
                //$btnNext.show();

                if ($current >= $total) {
                    $btnNext.hide();
                    //$btnFinish.show();
                } else {
                    $btnNext.show();
                    $btnFinish.hide();
                }
            },
            'onNext': function ($tab, $navigation, $index) {
                var valid_sku;
                var $valid;

                if ($('#step3').parent().hasClass('active')) {
                    valid_sku = $('.no-after').hasClass("sku-icon-svg");
                    $('.sku-error').removeClass('hidden');
                    $valid = valid_sku && $form1.valid();
                } else {
                    $valid = $form1.valid();
                }

                if ($('#step4').parent().hasClass('active')) {
                    if ($form1.valid()) {
                        summary();
                    }
                }

                if (!$valid) {
                    $validator1.focusInvalid();
                    return false;
                }

            },
            onTabClick: function ($tab, $navigation, $index) {
                return false;
            }
        });

        // Init wizard with validation
        //jQuery('.js-wizard-validation').bootstrapWizard({
        //    'tabClass': '',
        //    'previousSelector': '.wizard-prev',
        //    'nextSelector': '.wizard-next',
        //    'onTabShow': function ($tab, $nav, $index) {
        //        var $total = $nav.find('li').length;
        //        var $current = $index + 1;
        //
        //        // Get vital wizard elements
        //        var $wizard = $nav.parents('.block');
        //        var $btnNext = $wizard.find('.wizard-next');
        //        var $btnFinish = $wizard.find('.wizard-finish');
        //
        //        // If it's the last tab then hide the last button and show the finish instead
        //        if ($current >= $total) {
        //            $btnNext.hide();
        //            $btnFinish.show();
        //        } else {
        //            $btnNext.show();
        //            $btnFinish.hide();
        //        }
        //    },
        //    'onNext': function ($tab, $navigation, $index) {
        //        var $valid = $form2.valid();
        //
        //        if (!$valid) {
        //            $validator2.focusInvalid();
        //
        //            return false;
        //        }
        //    },
        //    onTabClick: function ($tab, $navigation, $index) {
        //        return false;
        //    }
        //});
    };

    return {
        init: function () {
            // Init simple wizard
            initWizardSimple();

            // Init wizards with validation
            initWizardValidation();


        }
    };
}();

// Initialize when page loads
jQuery(function () {
    BaseFormWizard.init();
});
