/*
 *  Document   : base_forms_validation.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Form Validation Page
 */

var BaseFormValidation = function () {
    // Init Material Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    $.validator.addMethod("selectCustomer", function(value, element, arg){
        return value != '';
    }, "Please select customer");

    var initValidationMaterial = function () {
        jQuery('.js-validation-material').validate({
            ignore: [],
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function (error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function (e) {
                var elem = jQuery(e);

                elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                elem.closest('.help-block').remove();
            },
            success: function (e) {
                var elem = jQuery(e);

                elem.closest('.form-group').removeClass('has-error');
                elem.closest('.help-block').remove();
            },
            rules: {
                'user[name]': {
                    required: true,
                    maxlength: 25
                },
                'user[email]': {
                    required: true
                },
                'user[password]': {
                    required: true,
                    minlength: 6,
                    maxlength: 12
                },
                'tags_info[ad_request]': {
                    required: true,
                    digits: true,
                    min: 0
                },
                'tags_info[match_request]': {
                    required: true,
                    digits: true,
                    min: 0
                },
                'tags_info[clicks]':{
                    required: true,
                    digits: true,
                    min: 0
                },
                'tags_info[ctr]': {
                    required: true,
                    digits: true,
                    min: 0
                },
                'tags_info[ecpm]':{
                    required: true,
                    number: true,
                    min: 0
                },
                'tags_info[partner_revenue]':{
                    required: true,
                    number: true,
                    min: 0
                },
                'tags_info[fill_rate]':{
                    required: true,
                    digits: true,
                    min: 0
                },
                'tags_info[frm_date]':{
                    required: true
                },
                'tags_info[to_date]':{
                    required: true
                },
                'tag[name]':{
                    required: true
                },
                'tag[user_id]': {
                    selectCustomer: "Please select"
                }
            },
            messages: {
                'user[name]': {
                    required: 'Please enter name',
                    maxlength: 'Should be less than 25 characters'
                },
                'user[email]': {
                    required: 'Please enter email'
                },
                'user[password]': {
                    required: 'Please enter password',
                    minlength: 'Password length should be minimum 6 characters.',
                    maxlength: 'Password cannot have more than 12 characters'
                },
                'tags_info[ad_request]': {
                    required: 'Please enter Ad Request',
                    digits: 'Please enter only digits',
                    min: 'Please enter value greater than 0'
                },
                'tags_info[match_request]': {
                    required: 'Please enter Match Request',
                    digits: 'Please enter only digits',
                    min: 'Please enter value greater than 0'
                },
                'tags_info[clicks]':{
                    required: 'Please enter',
                    digits: 'Please enter only digits',
                    min: 'Please enter value greater than 0'
                },
                'tags_info[ctr]': {
                    required: 'Please enter',
                    digits: 'Please enter only digits',
                    min: 'Please enter value greater than 0'
                },
                'tags_info[ecpm]':{
                    required: 'Please enter ECPM',
                    number: 'Please enter numeric values',
                    min: 'Please enter value greater than 0'
                },
                'tags_info[partner_revenue]':{
                    required: 'Please enter Partner Revenue',
                    number: 'Please enter numeric values',
                    min: 'Please enter value greater than 0'
                },
                'tags_info[fill_rate]':{
                    required: 'Please enter Fill Rate',
                    digits: 'Please enter only digits',
                    min: 'Please enter value greater than 0'
                },
                'tags_info[frm_date]':{
                    required: 'Please enter from From Date'
                },
                'tags_info[to_date]':{
                    required: 'Please enter to To Date'
                },
                'tag[name]':{
                    required: 'Please enter name'
                }
            }
        });
    };

    return {
        init: function () {
            // Init Bootstrap Forms Validation
            //initValidationBootstrap();

            // Init Material Forms Validation
            initValidationMaterial();

            // Init Validation on Select2 change
            jQuery('.jsSelect').on('change', function () {
                jQuery(this).valid();
            });

        }
    };
}();


// Initialize when page loads
jQuery(function () {
    BaseFormValidation.init();
});
