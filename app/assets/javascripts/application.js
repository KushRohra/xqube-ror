// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.min
//= require jquery_ujs
//= require bootstrap.min
//= require app
//= require jquery.placeholder.min
//= require jquery.scrollLock.min
//= require jquery.slimscroll.min
//= require js.cookie.min
//= require jquery.validate
//= require bootstrap-datepicker.min
//= require base_forms_validation
//= require reports
//= require jquery.backstretch.min
//= require background_script
//= require jquery-ui/widgets/autocomplete
//= require autocomplete-rails
//= require clipboard

$(document).ready(function () {

    // hide spinner
    $("#spinner").hide();


    // show spinner on AJAX start
    $(document).ajaxStart(function () {
        $("#spinner").show();
    });

    // hide spinner on AJAX stop
    $(document).ajaxStop(function () {
        $("#spinner").hide();
    });

});

$(document).on("page:load ready", function () {
    $("input.datepicker").datepicker();
});