module ReportsHelper

  def date_range_options
    date_range_options = [
        ['Custom Date Range', 'custom'],
        ['Today', 'today'],
        ['Yesterday', 'yesterday'],
        ['This Week(Sun-today)', 'this_week'],
        ['Past 7 days', 'past_7_days'],
        ['This Month(1st-today)', 'this_month'],
        ['Past 30 days', 'past_30_days'],
        ['Past Month', 'past_month'],
        ['Past 90 days', 'past_90_days'],
        ['This Year(Jan 1-today)', 'this_year'],
        ['Past 365 days', 'past_365_days']
    ]
    return date_range_options
  end

  def write_to_csv(tags_infos, summary)
    attributes = ['NAME', 'DATE',	'TOTAL IMPRESSIONS',	'PAID IMPRESSIONS',	'ECPM',	'FILL RATE',	'PARTNER REVENUE']
    CSV.generate(headers: true) do |csv|
      csv << attributes

      tags_infos.each do |tag_info|
        csv << [tag_info.tag.name,
                tag_info.frm_date,
                tag_info.ad_request.to_i,
                tag_info.match_request.to_i,
                tag_info.ecpm.present? ? "$ #{tag_info.ecpm.round(2)}" : '',
                tag_info.fill_rate.present? ? "#{tag_info.fill_rate} %" : '',
                tag_info.partner_revenue.present? ? "$ #{tag_info.partner_revenue.round(2)}" : '']
      end

      csv << ['', 'Total',
              summary[:total_ad_request],
              summary[:total_match_request],
              summary[:total_ecpm].present? ? "$ #{summary[:total_ecpm].round(2)}" : '',
              summary[:total_fill_rate].present? ? "#{summary[:total_fill_rate]} %" : '',
              summary[:total_partner_revenue].present? ? "$ #{summary[:total_partner_revenue].round(2)}" : ''
      ]
    end
  end
end
