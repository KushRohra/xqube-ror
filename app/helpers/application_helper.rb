module ApplicationHelper
  def bootstrap_class_for flash_type
    value = flash_type.to_s
    case flash_type
      when 'success'
        value = "alert-success"
      when 'error'
        value = "alert-danger"
      when 'alert'
        value = "alert-block"
      when 'notice'
        value = "alert-info"
    end
    return value
  end

  def number_to_currency_dollar(number)
    number_to_currency(number, :unit => '$ ',seperator: ',', delimiter: '', precision: 2, raise: true )
  end

  def number_to_percentage_fill(number)
    number_to_percentage(number,precision: 0,format: '%n  %', raise: true)
  end

=begin
  def paginate(collection, params= {})
    will_paginate collection, params.merge(:renderer => RemoteLinkPaginationHelper::LinkRenderer)
  end
=end

end
