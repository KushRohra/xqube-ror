class ApplicationMailer < ActionMailer::Base
  include Devise::Mailers::Helpers
  default from: 'Adsolut Media <info@adsolut.in>',
  :reply_to => 'info@adsolut.in'

  def email(site_request)
    @site_request = site_request
    mail(from: "Admin - Adsolut <info@adsolut.in>" ,to: 'info@adsolut.in', subject: 'Approve site')
  end

  def reset_password_instructions(record, token, opts={})
    @token = token
    devise_mail(record, :reset_password_instructions, opts)
  end

  def unlock_instructions(record)
    devise_mail(record, :unlock_instructions)
  end

  def confirmation_instructions(record)
    devise_mail(record, :confirmation_instructions)
  end

end