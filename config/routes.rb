Rails.application.routes.draw do
 
  namespace :api do
    post 'users/sign_in', controller: 'users#sign_in'
    delete 'users/sign_out', controller: 'users#sign_out'
    resources :tags, only: :index
    resources :tags_infos, only: :index
  end

  resources :payments

  devise_for :users, :path_prefix => 'authentication'

  resources :users, param: :encoded_id

  devise_scope :user do
    root to: 'devise/sessions#new'
  end

  get 'upload/tags_info'

  resources 'site-requests', :controller => :site_requests, :as => :site_requests

  resources :publishers, path: 'publisher/signup' do
    collection do
      get 'thank-you'
    end
  end

  resources :pages do
    post :search_index, :on => :collection
  end

  resources :tags, param: :encoded_id do
    collection do
      get ':encoded_id/export', to: 'tags#export', as: :export
    end
  end

  resources :tags_infos

  resources :uploads do
    collection { post :import }
  end

  resources :reports do
    get :download_csv, on: :collection
  end

  resources :sites do
    get :autocomplete_tag_name, :on => :collection
  end

  get 'ads_txt/download', to: 'ads_txt#download', controller: 'ads_txt'



#resources :tags do
#collection {post :upload}
#end

#get 'pages/upload_tags', to: 'pages#upload_tags'

#get 'tags', to: 'tags#index'

#resources :tags_info do
#collection {post :upload}
#end

#get 'pages/index'

# The priority is based upon order of creation: first created -> highest priority.
# See how all your routes lay out with "rake routes".

# You can have the root of your site routed with "root"
# root 'welcome#index'

# Example of regular route:
#   get 'products/:id' => 'catalog#view'

# Example of named route that can be invoked with purchase_url(id: product.id)
#   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

# Example resource route (maps HTTP verbs to controller actions automatically):
#   resources :products

# Example resource route with options:
#   resources :products do
#     member do
#       get 'short'
#       post 'toggle'
#     end
#
#     collection do
#       get 'sold'
#     end
#   end

# Example resource route with sub-resources:
#   resources :products do
#     resources :comments, :sales
#     resource :seller
#   end

# Example resource route with more complex sub-resources:
#   resources :products do
#     resources :comments
#     resources :sales do
#       get 'recent', on: :collection
#     end
#   end

# Example resource route with concerns:
#   concern :toggleable do
#     post 'toggle'
#   end
#   resources :posts, concerns: :toggleable
#   resources :photos, concerns: :toggleable

# Example resource route within a namespace:
#   namespace :admin do
#     # Directs /admin/products/* to Admin::ProductsController
#     # (app/controllers/admin/products_controller.rb)
#     resources :products
#   end

end