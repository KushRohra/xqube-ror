# frozen_string_literal: true

Swagger::Docs::Config.base_api_controller = ActionController::Base

Swagger::Docs::Config.register_apis({
                                        '1.0' => {
                                            # the extension used for the API
                                            api_extension_type: :json,
                                            # the output location where your .json files are written to
                                            api_file_path: 'public/apidocs',
                                            # the URL base path to your API
                                            base_path: Rails.application.secrets.base_url,
                                            # if you want to delete all .json files at each generation
                                            clean_directory: true,
                                            # add custom attributes to api-docs
                                            attributes: {
                                                info: {
                                                    title: 'API Documentation',
                                                    description: '',
                                                    termsOfServiceUrl: '',
                                                    contact: ''
                                                }
                                            }
                                        }
                                    })
