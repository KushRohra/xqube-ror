require_relative 'boot'

require 'rails/all'
require 'csv'
require 'iconv'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Workspace
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.serve_static_files = true
    #config.active_record.raise_in_transactional_callbacks = true
    config.assets.paths << Rails.root.join("app", "assets", "images", "fonts")
    config.action_mailer.delivery_method = :postmark
    config.action_mailer.postmark_settings = { :api_token => "52415227-f873-4418-97a5-b8a87c9bd72b" }
    config.assets.precompile += ["*.ttf", "*.woff", "*.svg", "*.eot"] 
  end
end