source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

#Ruby Version
ruby '2.5.1'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2', '>= 5.2.2'
# Use postgress as the database for Active Record
gem 'pg', '~> 0.18'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'execjs'

gem 'therubyracer'

gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
#gem 'coffee-rails', '~> 4.1', '>= 4.1.1'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
gem 'nokogiri', '~> 1.8.4'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
#gem for authentication
gem 'devise', '~> 4.2'
#gem for twitter bootstrap
gem 'twitter-bootstrap-rails', '~> 3.2', '>= 3.2.2'
#gem for devise views
gem 'devise-bootstrap-views', '~> 0.0.7'
#gem for excel,csv
gem 'roo'
#gem for excel
gem 'roo-xls', '~> 1.0'
#gem for iconv
gem 'iconv', '~> 1.0.3'
#gem for pagination
gem 'kaminari', '~> 0.17.0'
#gem for breadcrumb
gem 'breadcrumbs_on_rails'
#gem for google charts
gem "google_visualr", ">= 2.5"
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
gem 'spreadsheet', '~> 1.1', '>= 1.1.3'
#gem for roles
gem 'rolify', '~> 5.1.0'
#gem for authorization
gem 'pundit'
# Use Unicorn as the app server
# gem 'unicorn'
# Use for performance monitoring
gem 'newrelic_rpm'
# Use for email
gem 'postmark-rails'
# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
#gem for aws
gem 'aws-sdk', '~> 3'
gem 'aws-sdk-s3', '~> 1'
#gem for paperclip
gem 'paperclip'
#gem for autocomplete
gem 'rails-jquery-autocomplete'
#gem for jquery UI
gem 'jquery-ui-rails'
#gem for Meta tags
gem 'meta-tags'
#gem for copy clipboard
gem 'clipboard-rails'
# Use Puma as the app server
gem 'puma', '~> 3.7'
gem 'bootsnap', '~> 1.1', '>= 1.1.7'

# For API documentation
gem 'swagger-docs'

# For JSON web token
gem 'jwt'

# Serialize output
gem 'active_model_serializers', '~>0.9.4'

#gem for recaptcha
gem 'recaptcha', require: 'recaptcha/rails'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', '~> 10.0.2'
  # Gem for rails footnotes
  #gem 'rails-footnotes', '~> 4.0'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'pry'
  gem 'listen', '~> 3.1', '>= 3.1.5'
end

group :production do
  #Making Apps Running Eaiser
  gem 'rails_12factor'
end
